<?php

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\Exception\HttpMethodNotAllowedException;
use Phroute\Phroute\Exception\HttpRouteNotFoundException;

use App\Models\Auth;

function processInput($uri){
    $uri = urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

        return $uri;
}

function processOutput($response){
    echo $response;
}

$router = new RouteCollector();

$router->controller('/', 'App\Controllers\\PageController');
$router->controller('/error/{code:a}', 'App\Controllers\\PageController');
// $router->get('/error/{code:a}', ['App\Controllers\PageController', 'error']);

$router->post('/check-number', ['App\Controllers\PhpController', 'checkNumber']);
$router->post('/check-score', ['App\Controllers\PhpController', 'checkScore']);

$router->post('/show-array', ['App\Controllers\XmlController', 'showArray']);

$router->post('/show-tables', ['App\Controllers\MysqlController', 'showTables']);
$router->post('/user-off', ['App\Controllers\MysqlController', 'userOff']);
$router->post('/more-one', ['App\Controllers\MysqlController', 'moreOne']);



$dispatcher =  new Dispatcher($router->getData());

try {

    $response = $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], processInput($_SERVER['REQUEST_URI']));

} catch (HttpRouteNotFoundException $e) {

    // var_dump($e->getMessage());
    // die();
    $response = $dispatcher->dispatch('GET', '/error/404');
    

} catch (HttpMethodNotAllowedException $e) {

    // var_dump($e->getMessage());
    // die();
    $response = $dispatcher->dispatch('GET', '/error/405');

}

processOutput($response);