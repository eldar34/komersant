//Глобальная регистрация компонента (модальное окно)

Vue.component('app-modal', {
    props: ['result'],
    template: `
<!-- Modal -->
<div id="exampleModalLong" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Array Result</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <code>
         {{result}}
        </code>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
      </div>
    </div>
  </div>
</div>                                
`
});

Vue.component('app-modal-couple', {
    props: ['dataList', 'showModalLoader'],
    data(){
        return{

        }
    },
    template: `
<!-- Modal -->
<div id="exampleModalLongCouple" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongCouple" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">User - Office</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-sm">
            <thead>
                <tr>
                    <th scope="col">Username</th>
                    <th scope="col">Office</th>                                  
                </tr>
            </thead>                             
            <tbody>
                <tr v-for="(val, index) in dataList">
                    <td>{{ val.username }}</td>
                    <td>{{ val.office }}</td>
                </tr>                          
            </tbody>
        </table>
        <div class="col-md-12 text-center">
            <div v-if="showModalLoader" class="spinner-border text-center" role="status">
                      <span class="sr-only">Loading...</span>
            </div>
        </div>
      </div>
      <div class="modal-footer">      
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
      </div>
    </div>
  </div>
</div>                                
`
});

Vue.component('app-modal-friends', {
    props: ['dataList', 'showModalLoader'],
    data(){
      return{

      }
    },
    template: `
<!-- Modal -->
<div id="exampleModalLong" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">More One</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-sm">
            <thead>
                <tr>
                    <th scope="col">Office</th>                                   
                </tr>
            </thead>                             
            <tbody>
                <tr v-for="(val, index) in dataList">
                    <td>{{ val.office }}</td>
                </tr>                          
            </tbody>
        </table>
        <div class="col-md-12 text-center">
            <div v-if="showModalLoader" class="spinner-border text-center" role="status">
                      <span class="sr-only">Loading...</span>
            </div>
        </div>
      </div>
      <div class="modal-footer">      
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>       
      </div>
    </div>
  </div>
</div>                                
`
});

//Компоненты для Home Page

const HomePage = {
    template: `
<div class="row">
    <div class="col-md-4 mt-5">
        <p class="text-center">
        Задание 1
        <br><br>Напишите код (функцию, класс), которая проверяет простое число или нет
        На вход - число, выход - да/нет
        <br><br>Задание 2
        <br><br>Есть счет, в котором указана сумма с НДС, есть дата счета и все прочие атрибуты.
        Напишите класс, в нем метод - в который на вход поступает информация о счете, 
        на выходе - стоимость без НДС (страна - Российская Федерация)
        </p>

    </div>
    <div class="col-md-4 mt-5">
        <p class="text-center">
        Задание 1
        <br><br>Найдите ошибки в файле в приложении
        <br><br>Задание 2
        <br><br>Напишите код (на PHP) выдающий <br>массив данных: 
        <br>- Должник: id, тип, имя
        <br>- Список лотов: по каждому - номер, стоимость, описание.          
        </p>
    </div>
    <div class="col-md-4 mt-5">
        <p class="text-center">
        Задание 1
        <br><br>Перечислите, чтобы вы поправили в таблицах (тип данных, название, длину - что угодно - и почему)
        <br><br>Задание 2
        <br><br>Выведите имена пользователей и названия офисов, в которых они сидят
        <br><br>Задание 3
        <br><br>Выведите названия офисов, в котором сидят больше, чем один пользователь           
        </p>
    </div>
</div>
`
};

//Компоненты для Задания 1

const First = {
    data(){
      return{
          hasError: false,
          simpleNumber: '',
          resultPrime: '',
          errors: {},
          ndsTotal: '',
          resultPrise: '',
          showLoader: false,
          showModalLoader: true
      }
    },
    
    template: `
<div class="row">
    <div class="col-md-6 mt-5 text-center">
    <p>Задание 1</p>
        <div class="row">
            <div class="col-md-10 offset-md-2">
                <form @submit.prevent="submitSimple" class="form-inline">
                    <div class="form-group row">
                        <div class="col-md-8 offset-md-1">
                            <input 
                                type="text" 
                                v-model="simpleNumber" 
                                name="simplenumber" 
                                class="form-control" 
                                placeholder="Simple Number"
                                :class="errors.hasOwnProperty('simplenumber') ? 'is-invalid': ''"
                            >
                            <div class="invalid-feedback">
                                {{ errors.simplenumber }}
                            </div>
                        </div>

                    
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary mb-2">Send</button>
                        </div>   
                    </div> 
                                               
                </form>
                <div class="col-md-6 form-group mt-4 offset-md-1">
                    <label for="primeResult">Is Prime:</label> 
                    <p id="primeResult" :style="errorMassage">{{ resultPrime }}</p>
                                                  
                </div>
            </div>
        </div>
                        
    </div>
    <div class="col-md-4 mt-5 offset-md-2 text-center">
    <p>Задание 2</p>
        <div class="row">
            <form @submit.prevent="ndsPrice" class="form-inline">
              <div class="form-group row">
                <div class="col-md-8 offset-md-2">
                     <input 
                        type="text" 
                        v-model="ndsTotal" 
                        name="ndstotal" 
                        class="form-control" 
                        placeholder="NDS Price"
                        :class="errors.hasOwnProperty('ndstotal') ? 'is-invalid': ''"
                      >
                      <div class="invalid-feedback">
                                {{ errors.ndstotal }}
                      </div>
                </div>
                
                <div class="col-md-2">
                     <button type="submit" class="btn btn-primary mb-2">Send</button>
                </div>   
              </div>                              
            </form>
            
        </div>
        <div class="col-md-6 form-group mt-4 offset-md-3">
                    <label for="priceResult">Without NDS:</label> 
                    <p id="priceResult" :style="errorMassage">{{ resultPrise }}</p>
                                                  
        </div>
        <div class="row">
            <div class="col-md-6 mt-5">
                <p class="text-left">
                    <router-link to="/">HomePage</router-link>
                </p>
            </div>
        </div>
    </div>
</div>     
`,
    methods: {
        ndsPrice(){
            $.ajax({
                type: 'POST',
                url: '/check-score',
                data: {ndstotal: this.ndsTotal},
                success: function(data) {
                    var result = JSON.parse(data);

                    if(result.status == 'success'){

                        this.errors = {};
                        this.resultPrise = result.data;
                       
                    }else{
                        this.validationText(result);
                    }
                }.bind(this),
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        },

        submitSimple(){
            $.ajax({
                type: 'POST',
                url: '/check-number',
                data: {simplenumber: this.simpleNumber},
                success: function(data) {
    
                    var result = JSON.parse(data);

                    if(result.status == 'success'){
                        this.errors = {};

                        if(result.data == 'true'){
                            this.resultPrime = 'true';
                            this.hasError = false;
                        }else{
                            this.resultPrime = 'false';
                            this.hasError = true;
                        }
                       
                    }else{
                        this.validationText(result);
                    }
                }.bind(this),
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        },

        validationText(errors) {
            let currentObj = {};
            errors.forEach(function (item, i, arr) {
                let gj = item.field;
                if (item.status == 'error') {
                    
                    let fieldName = $('label[for*=' + gj + ']').text();

                    currentObj[gj] = item.errors[0];
                } 
            });
            
            this.errors = currentObj;
          },

    },
    computed: {
        
        errorMassage(){
            return this.hasError ? 'color: red' : 'color: #2bf94e';
        }

}
};



//Компонент для Задания 2 (форма)

const Second = {
    data(){
        return{
            
            hasError: false,
            errors: '',
            showModal: false,
            resultArray: [],
            tetst: xmlDoc
            
        }
    },
    template: `


<div class="row">
        
        <div class="col-md-10 mt-5 text-center">
            <div class="row">
                <div class="col-md-12">
                    <pre>
                        {{ tetst }}
                    </pre>
                </div>
            </div>

        </div>
        <div class="col-md-2 mt-5  text-center">


            <div class="row">
            <app-modal :result="modalProps"></app-modal>
                <div class="col-md-12 mt-2 right">
                    <p>Задание 2</p>
                    <button @click="onSubmit" class="btn btn-primary">Send</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 mt-5">
                    <p class="text-left">
                        <router-link to="/">HomePage</router-link>
                    </p>
                </div>
            </div>
        </div>
    </div>
`,
    methods: {
        onSubmit(){
            $.ajax({
                type: 'POST',
                url: '/show-array',
                success: function(data) {
                    var result = JSON.parse(data);

                    if(result.status == 'error'){
                        this.errors = result.error;
                        this.hasError = true;
                    }else{
                        this.hasError = false;
                        this.resultArray = result;
                        $('#exampleModalLong').modal();
                    }
                }.bind(this),
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        }
    },
    computed: {       
        modalProps () {
            return this.resultArray;
        }
    }
};

//Компоненты для Задания 3

const Third = {
    data(){
      return{
          users: '',
          offices: '',
          hasError: false,
          errors: '',
          showLoader: false,
          more5friends: '',
          showModalLoader: true
      }
    },
    beforeRouteEnter (to, from, next) {
        next(vm => {
            // экземпляр компонента доступен как `vm`
            vm.showLoader = true;
            $.ajax({
                type: 'POST',
                url: '/show-tables',
                success: function(data) {
                    var result = JSON.parse(data);
                    vm.showLoader = false;

                    if(result.status == 'success'){
                        vm.users = result.users;
                        vm.offices = result.offices;

                    }else{
                        vm.hasError = true;
                        vm.users = 'some';
                    }
                }.bind(vm),
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });

        })
    },
    template: `
<div class="row">
<app-modal-couple :dataList="more5friends" :showModalLoader="showModalLoader"></app-modal-couple>
<app-modal-friends :dataList="more5friends" :showModalLoader="showModalLoader"></app-modal-friends>
    <div class="col-md-6 mt-5 text-center">
        <div class="row">
            <div class="col-md-4">
                <p>Users Table</p>
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Name</th>                      
                      <th scope="col">Office</th>                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="user in users">
                      <th scope="row">{{ user.id }}</th>
                      <td>{{ user.name }}</td>
                      <td>{{ user.office_id }}</td>
                    </tr>                    
                  </tbody>
                </table>
                <div v-if="showLoader" class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
            </div>
            <div class="col-md-8">
            <p>Offices Table</p>
                <table class="table table-sm">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Name</th>                                           
                    </tr>
                  </thead>
                  <tbody>
                    <tr v-for="office in offices">
                      <th scope="row">{{ office.id }}</th>
                      <td>{{ office.name }}</td>
                    </tr>                    
                  </tbody>
                </table>
                <div v-if="showLoader" class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
                        
    </div>
    <div class="col-md-5 mt-5 offset-md-1 text-center">
    
        
        <div class="row">
        
            <div class="col-md-6 mt-2">
            <p>Задание 2</p>
                     <button @click="userOff" class="btn btn-primary">Send</button>
            </div>
            <div class="col-md-6 mt-2">
            <p>Задание 3</p>
                     <button @click="moreOne" class="btn btn-primary">Send</button>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mt-5">
                <p class="text-left">
                    <router-link to="/">HomePage</router-link>
                </p>
            </div>
        </div>
    </div>
</div>     
`,
    methods: {

        moreOne(){
            $('#exampleModalLong').modal();
            $.ajax({
                type: 'POST',
                url: '/more-one',
                success: function(data) {
                    var result = JSON.parse(data);
                    if(result.status == 'success'){
                        this.showModalLoader = false;
                        this.more5friends = result.datalist;
                    }else{
                        //this.errors = ' ' + result.error.toString().replace(/'|"/, '') + ' ';
                        //this.hasError = true;
                    }
                }.bind(this),
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        },

        userOff(){
            
            $('#exampleModalLongCouple').modal();
            $.ajax({
                type: 'POST',
                url: '/user-off',
                success: function(data) {
                    var result = JSON.parse(data);
                    if(result.status == 'success'){
                        this.showModalLoader = false;
                        this.more5friends = result.datalist;
                        
                    }else{
                        //this.errors = ' ' + result.error.toString().replace(/'|"/, '') + ' ';
                        //this.hasError = true;
                    }
                }.bind(this),
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            });
        }
    }
};

// Связываем маршруты с компонентами

const routes = [
    { path: '/', component: HomePage },
    { path: '/first', component: First },
    { path: '/second', component: Second },
    { path: '/third', component: Third }
];

// Передаём routes во VueRoutes routes: routes

const router = new VueRouter({
    routes
});

// Создаем экземпляр Vue

new Vue({
    el: '#app',
    router,
    data: {

    },
});



const xmlDoc = `
<?xml version="1.0" encoding="utf-8"?>
<MessageData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <Id>4610237</Id>
    <Number>4610237</Number>
    <CaseNumber>А60-52850/2018 </CaseNumber>
    <PublisherInfo PublisherType="ArbitrManager">
        <ArbitrManager Id="1" FirstName="Лариса" MiddleName="Анатольевна" LastName="Касьянова" INN="123456789012" SNILS="1234567890" RegistryNumber="124">
            <Sro SroId="14">
                <SroName>Ассоциация "РО АУ"</SroName>
                <SroRegistryNumber>123-12</SroRegistryNumber>
            </Sro>
            <CorrespondenceAddress>620000, Екатеринбург, пр. Ленина</CorrespondenceAddress>
        </ArbitrManager>
    </PublisherInfo>
    <MessageInfo MessageType="Auction">
        <Auction>
            <Text>
                Организатор торгов Финансовый управляющий Л.А. Касьянова, действующая на основании решения Арбитражного суда Свердловской области от 13.04.2017
                года по делу № А12-12345/2018, сообщает о продаже имущества Сердюкова Андрея Петровича в форме повторных торгов на электронной площадке ООО
                «ЮТендер» в сети Интернет на сайте www.utender.ru.
            </Text>
            <Date>2020-03-12T12:00:00</Date>
            <TradeType>OpenedAuction</TradeType>
            <PriceType>Public</PriceType>
            <Application>
                <TimeBegin>2020-01-31T13:00:00</TimeBegin>
                <TimeEnd>2020-03-07T13:00:00</TimeEnd>
            </Application>
            <TradeSite>uTender</TradeSite>
            <IdTradePlace>109</IdTradePlace>
            <LotTable>
                <AuctionLot>
                    <Order>1</Order>
                    <StartPrice>3129000</StartPrice>
                    <Step>5.00</Step>
                    <Advance>10</Advance>
                    <Description>
                        Лот № 1 - Комплекс имущества, расположенный по адресу: РФ, Свердловская обл., г.Алапаевск
                    </Description>
                    <AuctionStepUnit>Percent</AuctionStepUnit>
                    <AdvanceStepUnit>Percent</AdvanceStepUnit>
                </AuctionLot>
                <AuctionLot>
                    <Order>2</Order>
                    <StartPrice>7560000</StartPrice>
                    <Step>5.00</Step>
                    <Advance>10</Advance>
                    <Description>
                        Комплекс имущества, расположенный в п. Нейвинский г. Алапаевск
                    </Description>
                    <AuctionStepUnit>Percent</AuctionStepUnit>
                    <AdvanceStepUnit>Percent</AdvanceStepUnit>
                    <ClassifierCollection>
                        <AuctionLotClassifier>
                            <Code>0101017</Code>
                            <Name>Здания (помещения) жилые, не входящие в жилищный фонд</Name>
                        </AuctionLotClassifier>
                    </ClassifierCollection>
                </AuctionLot>
            </LotTable>
            <Participants />
            <AdditionalText>Размер задатка – 10 % от начальной цены лота. Реквизиты для перечисления задатка: Получатель: Сердюков Андрей Петрович</AdditionalText>
        </Auction>
    </MessageInfo>
    <BankruptInfo BankruptType="Person" BankruptCategory="SimplePerson">
        <BankruptPerson Id="114671" InsolventCategoryName="Физическое лицо" FirstName="АНДРЕЙ" MiddleName="ПЕТРОВИЧ" LastName="СЕРДЮКОВ" Address="123456, РОССИЯ, г Самара, ул Ленина, 12, 6">
            <Birthplace>Россия, г Иваново</Birthplace>
            <NameHistory>
                <NameHistoryItem>СЕРДЮКОВ АНДРЕЙ ПЕТРОВИЧ</NameHistoryItem>
            </NameHistory>
        </BankruptPerson>
    </BankruptInfo>
    <PublishDate>2020-01-30T20:46:20.173</PublishDate>
    <MessageURLList>
        <MessageURL URLName="Договор о задатке.docx" URL="http://bankrot.fedresurs.ru/Download/file.fo?id=2644644&amp;type=MessageDocument" DownloadSize="36674" />
        <MessageURL URLName="Проект договора купли-продажи.doc" URL="http://bankrot.fedresurs.ru/Download/file.fo?id=2644680&amp;type=MessageDocument" DownloadSize="48640" />
    </MessageURLList>
    <FileInfoList>
        <FileInfo>
            <Name>Договор о задатке.docx</Name>
        </FileInfo>
        <FileInfo>
            <Name>Проект договора купли-продажи.doc</Name>
        </FileInfo>
    </FileInfoList>
</MessageData>
`;