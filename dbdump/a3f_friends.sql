-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 28 2020 г., 11:09
-- Версия сервера: 5.7.21-20-beget-5.7.21-20-1-log
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `i966172c_todo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `a3f_friends`
--
-- Создание: Мар 08 2020 г., 15:35
--

DROP TABLE IF EXISTS `a3f_friends`;
CREATE TABLE `a3f_friends` (
  `id` int(11) NOT NULL,
  `friend_one` int(11) NOT NULL DEFAULT '0',
  `friend_two` int(11) NOT NULL DEFAULT '0',
  `status` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `a3f_friends`
--

INSERT INTO `a3f_friends` (`id`, `friend_one`, `friend_two`, `status`) VALUES
(1, 12, 9, '2'),
(2, 15, 10, '2'),
(3, 12, 19, '1'),
(4, 12, 18, '1'),
(5, 12, 17, '1'),
(6, 12, 16, '1'),
(7, 12, 15, '1'),
(8, 19, 9, '2'),
(9, 18, 9, '2'),
(10, 17, 9, '2'),
(11, 16, 9, '2'),
(12, 15, 9, '2'),
(19, 12, 4, '1'),
(20, 9, 1, '1'),
(21, 9, 2, '1'),
(22, 9, 3, '1'),
(23, 9, 4, '1'),
(24, 9, 5, '1'),
(25, 9, 6, '1'),
(26, 20, 12, '2'),
(27, 3, 12, '2');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `a3f_friends`
--
ALTER TABLE `a3f_friends`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK__a3f_users` (`friend_one`),
  ADD KEY `FK__a3f_users_2` (`friend_two`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `a3f_friends`
--
ALTER TABLE `a3f_friends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `a3f_friends`
--
ALTER TABLE `a3f_friends`
  ADD CONSTRAINT `FK__a3f_users` FOREIGN KEY (`friend_one`) REFERENCES `a3f_users` (`id`),
  ADD CONSTRAINT `FK__a3f_users_2` FOREIGN KEY (`friend_two`) REFERENCES `a3f_users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
