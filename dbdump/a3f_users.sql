-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 28 2020 г., 11:10
-- Версия сервера: 5.7.21-20-beget-5.7.21-20-1-log
-- Версия PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `i966172c_todo`
--

-- --------------------------------------------------------

--
-- Структура таблицы `a3f_users`
--
-- Создание: Мар 08 2020 г., 15:35
--

DROP TABLE IF EXISTS `a3f_users`;
CREATE TABLE `a3f_users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `a3f_users`
--

INSERT INTO `a3f_users` (`id`, `name`) VALUES
(1, 'John'),
(2, 'Jane'),
(3, 'Kristi'),
(4, 'Joahn'),
(5, 'Kyrie'),
(6, 'Smith'),
(7, 'Tom'),
(8, 'Din'),
(9, 'Kane'),
(10, 'Jes'),
(11, 'Key'),
(12, 'Dany'),
(13, 'Viny'),
(14, 'Mick'),
(15, 'Lian'),
(16, 'Pall'),
(17, 'Rick'),
(18, 'Kate'),
(19, 'Moony'),
(20, 'Nick');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `a3f_users`
--
ALTER TABLE `a3f_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `a3f_users`
--
ALTER TABLE `a3f_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
