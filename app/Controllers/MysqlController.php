<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\SimpleMysql;


class MysqlController extends Controller {

    public function showTables()
    {
        $simple_mysql = new SimpleMysql();
        $response = $simple_mysql->show_tables();

        return json_encode($response);
        
    }

    public function userOff()
    {
        $simple_mysql = new SimpleMysql();
        $response = $simple_mysql->user_off();

        return json_encode($response);
        
    }

    public function moreOne()
    {
        $simple_mysql = new SimpleMysql();
        $response = $simple_mysql->more_one();

        return json_encode($response);
        
    }

    
}