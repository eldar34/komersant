<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\Pagination;

class PageController extends Controller {

    public function getIndex()
    {        
        return $this->twig->render('index.html', [
            
            ]);        
    }

    public function getError($errorCode)
    {
        switch($errorCode){
            case '405':
                $code = '405';
                $errorTitle = ' Method Not Allowed';
                $message = 'Requested method not allowed!';
            break;

            case '503':
                $code = '503';
                $errorTitle = ' Service Unavailable';
                $message = 'Something wrong with connection to Database!';
            break;

            default:
                $code = '404';
                $errorTitle = ' Not Found';
                $message = 'Sorry, an error has occured. Requested page not found!';
        }

        return $this->twig->render('error.html', [
            'code' => $code, 
            'errorTitle' => $errorTitle, 
            'message' => $message, 
            ]); 
        

     }

     /**
     * 
     * Helper for fields vith html tags
     * 
     * @param string $stringTag
     * @return string
     */

    private function tagsHelper($stringTag){
        $result = html_entity_decode($stringTag, ENT_QUOTES | ENT_HTML5, "UTF-8");        
        return $result;
     }

}