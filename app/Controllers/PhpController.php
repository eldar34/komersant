<?php

namespace App\Controllers;

use App\Controllers\Controller;

use App\Models\Validate;
use App\Models\SimpleNumber;
use App\Models\SimpleScore;

class PhpController extends Controller {

    public function checkNumber()
    {
        if (isset($_POST['simplenumber'])) {
            $simplenumber = $_POST['simplenumber'];
        }

        $result = [];

        $validate = new Validate();

        $validNumber = $validate->validateNumber('simplenumber', $simplenumber);
        array_push($result, $validNumber);

        // Checking validation results
        $status_arr = array_column($result, 'status');

        if (in_array('error', $status_arr)) {
            return json_encode($result);            
        }else{
            $simpleNumber = new SimpleNumber();

            if($simpleNumber->is_prime($simplenumber)){
                $response['status'] = 'success';
                $response['data'] = 'true';
            }else{
                $response['status'] = 'success';
                $response['data'] = 'false';
            }
            return json_encode($response);
        }
    }

    public function checkScore()
    {
        if (isset($_POST['ndstotal'])) {
            $ndstotal = $_POST['ndstotal'];
        }

        $result = [];

        $validate = new Validate();

        $validNumber = $validate->validateNumber('ndstotal', $ndstotal);
        array_push($result, $validNumber);

        // Checking validation results
        $status_arr = array_column($result, 'status');

        if (in_array('error', $status_arr)) {
            return json_encode($result);            
        }else{
            $simpleScore = new SimpleScore(1, $ndstotal);

            $response['status'] = 'success';
            $response['data'] =  $simpleScore->without_nds($simpleScore->price);

            return json_encode($response);
        }
    }
}