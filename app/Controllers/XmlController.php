<?php

namespace App\Controllers;

use App\Controllers\Controller;

class XmlController extends Controller {

    public function showArray()
    {
        $result = [];

        $doc = new \DOMDocument();
        $doc->load('test.xml');

        $person = $doc->getElementsByTagName('ArbitrManager')->item(0);

        $result['person']['id'] = $doc->getElementsByTagName('Id')->item(0)->nodeValue;
        $result['person']['type'] = $doc->getElementsByTagName('PublisherInfo')->item(0)->getAttribute('PublisherType');
        $result['person']['name'] = $person->getAttribute('FirstName');
            
        $lots_table = $doc->getElementsByTagName('AuctionLot');

        $lot_list = [];

        foreach($lots_table as $val){
            $lot['number'] = $val->getElementsByTagName('Order')->item(0)->nodeValue;
            $lot['price'] = $val->getElementsByTagName('StartPrice')->item(0)->nodeValue;
            $lot['description'] = $val->getElementsByTagName('Description')->item(0)->nodeValue;

            array_push($lot_list, $lot);
        }

        $result['lot_list'] = $lot_list;

        return json_encode($result);

        
    }

    
}