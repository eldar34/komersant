<?php

namespace App\Models;

class Validate
{
    
    /**
     * Validate url adress
     * @param string $fieldName
     * @param integer $value
     * @return array $result
     */

    public function validateNumber($fieldName, $props){
        
        $response = [];
        $errors = [];
        $pattern = '#^[0-9]+$#';

        if(preg_match($pattern, $props)){
            $response['status'] = 'success';
            $response['field'] = $fieldName;
        }
        else{
            array_push($errors,' must be a number');
            array_push($errors,' должно быть числом');
            $response['status'] = 'error';
            $response['field'] = $fieldName;
            $response['errors'] = $errors;
        }
        return $response;  
    }

}