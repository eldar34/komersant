<?php

namespace App\Models;

/**
 * Sort class
 */

class SimpleNumber
{
    /**
     * Check value
     * @param integer $n
     * @return bool $result
     */

    public function is_prime($n)
    {
        if($n > 1){
            for($x=2; $x <= sqrt($n); $x++) {
                if($n % $x == 0) {
                    return false;
                }
            }
            return true;
        }else{
            return false;
        }
        
    }

}