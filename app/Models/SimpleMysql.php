<?php

namespace App\Models;

/**
 * SimpleMysql class
 */

use App\Models\Connection;

class SimpleMysql
{
    /**
     * Show Tables
     * @return array $resonse
     */

    public function show_tables()
    {
        $connection = new Connection();
        $pdo = $connection->dbConnect();

        $users = $pdo->query("SELECT * FROM users")->fetchAll();
        $offices = $pdo->query("SELECT * FROM offices")->fetchAll();

        $resonse['status'] = 'success';
        $resonse['users'] = $users;
        $resonse['offices'] = $offices;

        return $resonse;
        
    }

    public function user_off()
    {
        $connection = new Connection();
        $pdo = $connection->dbConnect();

        $query = "
            SELECT u.name AS username, o.name AS office 
            FROM users AS u
            JOIN offices AS o ON u.office_id = o.id
        ";

        $data = $pdo->query($query)->fetchAll();

        $resonse['status'] = 'success';
        $resonse['datalist'] = $data;
        

        return $resonse;
        
    }

    public function more_one()
    {
        $connection = new Connection();
        $pdo = $connection->dbConnect();

        $query = "
            SELECT o.name AS office 
            FROM offices AS o
            JOIN users AS u ON o.id = u.office_id
            GROUP BY o.name
            HAVING COUNT(o.name)>1
        ";

        $data = $pdo->query($query)->fetchAll();

        $resonse['status'] = 'success';
        $resonse['datalist'] = $data;
        

        return $resonse;
        
    }

}