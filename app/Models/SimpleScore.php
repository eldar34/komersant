<?php

namespace App\Models;

/**
 * Sort class
 */

class SimpleScore
{
    /**
     * Check value
     * @param integer $n
     * @return bool $result
     */

    private const NDS = 20;

    public $price;
    public $type;

    function __construct($type, $price)
    {
        $this->type = $type;
        $this->price = $price;
    }

    public function without_nds($price){
        $nds = ($price * self::NDS) / 100;
        $result = $price - $nds;
        return $result;
    }

}